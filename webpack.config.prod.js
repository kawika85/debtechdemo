var path = require('path');
var webpack = require('webpack');
var argv = require('minimist')(process.argv.slice(2));

module.exports = {
  devtool: 'source-map',
  node: {
    fs: "empty"
  },
  entry: [
    './public/src/index'
  ],
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  resolve: {
   extensions: ['', '.js', '.jsx', 'index.js', 'index.jsx', '.json', 'index.json'],
   modulesDirectories: ['web_modules', 'bower_components', 'node_modules'],

  },
  plugins: [
    new webpack.DefinePlugin({
      API_ENDPOINT: JSON.stringify(argv.endpoint || 'http://localhost:3030/'),
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    }),
    new webpack.ProvidePlugin({
      //'Promise': 'es6-promise', // Thanks Aaron (https://gist.github.com/Couto/b29676dd1ab8714a818f#gistcomment-1584602)
      'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
    })
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['babel'],
      include: path.join(__dirname, 'public/src'),
      exclude: path.join(__dirname, 'node_modules')
    },
    { test: /\.json$/, loader: 'json', include: path.join(__dirname, 'node_modules'),}
  ]


  }
};
