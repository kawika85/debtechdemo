'use strict';

const service = require('feathers-mongoose');
const recipient = require('./recipient-model');
const hooks = require('./hooks');



module.exports = function() {
  const app = this;

  const options = {
    Model: recipient,
    paginate: {
      default: 100,
      max: 200
    }
  };
  
  app.use('/recipients', service(options));
  const recipientService = app.service('/recipients');
  recipientService.before(hooks.before);
  recipientService.after(hooks.after);
};
