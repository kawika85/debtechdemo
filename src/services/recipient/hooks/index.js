'use strict';

const globalHooks = require('../../../hooks');
const hooks = require('feathers-hooks');
const authHooks = require('feathers-authentication').hooks;


exports.before = {
  all: [],
  find: [],
  get: [],
  create: [
    authHooks.verifyToken(),
    authHooks.restrictToAuthenticated()
  ],
  update: [],
  patch: [],
  remove: []
};

exports.after = {
  all: [hooks.remove('__v')],
  get: [],
  find: [],
  create: [],
  update: [],
  patch: [],
  remove: []
};
