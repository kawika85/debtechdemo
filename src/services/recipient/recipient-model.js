'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RecipientSchema = require('../schemas/recipient')

const recipientModel = mongoose.model('recipient', RecipientSchema);

module.exports = recipientModel;
