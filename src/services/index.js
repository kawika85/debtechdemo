'use strict';
const authentication = require('./authentication');
const mongoose = require('mongoose');

const user = require('./user');
const invoice = require('./invoice');
const recipient = require('./recipient');


module.exports = function() {
  const app = this;

  mongoose.connect(app.get('mongodb'));
  mongoose.Promise = global.Promise;

  app.configure(authentication);
  app.configure(user);
  app.configure(recipient);
  app.configure(invoice);

};
