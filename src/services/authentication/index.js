'use strict';

const authentication = require('feathers-authentication');


module.exports = function() {
  const app = this;

  //let config = app.get('auth');
  let config = {
    token: {
      secret: 'adsdasda7s6f9as98f787asf'
    },
    local: {
      usernameField: 'name'
    },
  }

  
  app.configure(authentication(config));
};
