'use strict';

const mongoose = require('mongoose');
const InvoiceSchema = require('../schemas/invoice')

const invoiceModel = mongoose.model('invoice', InvoiceSchema);

module.exports = invoiceModel;
