'use strict';

const globalHooks = require('../../../hooks');
const hooks = require('feathers-hooks');
const authHooks = require('feathers-authentication').hooks;

const filesHook = () => function(hook) {
  hook.data.files = hook.params.files
  return hook
  
}
const recipientHook = () => function(hook, next) {
  const recipients = hook.app.services.recipients
  const newRecipient = JSON.parse(hook.data.recipient)
  
  recipients.create(newRecipient).then(
    (data) => {
        hook.data.recipient = data._id
        next()        
    }
  )
  
}

const emitCreatedHook = () => function(hook) {
  const socket = hook.app.io
  socket.emit('action', {type:'INVOICE:NEW', data: hook.result});
  return hook
}

//DRY
//feathers doesn't support populate + pluck/remove together
//https://github.com/feathersjs/feathers-hooks/issues/68
const filterUserData = () => function(hook) 
{
    const pluckUser = (o) => {
      o.user = {_id:o.user._id, name: o.user.name }
      return o
    }
    const {data} = hook.result
    //result can come from find() or get()
    if(data){
      data.map( pluckUser)
      hook.result.data = data
    }else{
      hook.result = pluckUser(hook.result) 
    }
    
    return hook
}

exports.before = {
  all: [],
  find: [],
  get: [],
  create: [
    authHooks.verifyToken(),
    authHooks.populateUser(),
    authHooks.restrictToAuthenticated(),
    authHooks.associateCurrentUser({ idField: '_id', as: 'sentBy' }),
    recipientHook(),
    filesHook()
  ],
  update: [],
  patch: [],
  remove: []
};

const safeGetHooks = [
  hooks.populate('user', {
    service: 'users',
    field: 'sentBy',
    fieldName: '_id'
  }),
  hooks.remove('__v', 'sentBy'),
  filterUserData()
]

const populateRecipientHooks = [
  hooks.populate('recipient', {
    service: 'recipients',
    field: 'recipient',
    fieldName: '_id'
  })
  //TODO remove __, _id from associated recipient
  //,hooks.remove('__v', '_id')
]
exports.after = {
  all: [hooks.remove('__v')],
  get: [...safeGetHooks, ...populateRecipientHooks],
  find: [...safeGetHooks, ...populateRecipientHooks],
  create: [ ...safeGetHooks, ...populateRecipientHooks, emitCreatedHook() ], //[...safeGetHooks],
  update: [],
  patch: [],
  remove: []
};
