'use strict';

const service = require('feathers-mongoose');
const invoice = require('./invoice-model');
const hooks = require('./hooks');

const multer = require('multer');
const multipartMiddleware = multer();
const util = require('util');

const fs = require('fs');
const async = require('async');


module.exports = function() {
  const app = this;

  const options = {
    Model: invoice,
    paginate: {
      default: 100,
      max: 200
    }
  };
  
  const blobStorageMiddleware = (req,res,next) => {
      req.feathers.files = []
      async.eachSeries( 
        req.files, 
        (file, cb) => {
          //TODO store file with uid String, not the original name
          fs.writeFile(`uploads/${file.originalname}`, file.buffer, (err) => {
            if (!err) {
              req.feathers.files.push(file.originalname)
            }
            cb(err);
          });
        },
        function(err) {
          next()
        }
      );
    }
  
  app.use('/invoices',
    multipartMiddleware.any(),
    blobStorageMiddleware,
    service(options)
  )
  
  const invoiceService = app.service('/invoices');
  invoiceService.before(hooks.before);
  invoiceService.after(hooks.after);
};
