'use strict';

// user-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = require('../schemas/user')

const userModel = mongoose.model('user', UserSchema);

module.exports = userModel;
