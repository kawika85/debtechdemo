'use strict';

const globalHooks = require('../../../hooks');
const hooks = require('feathers-hooks');
const auth = require('feathers-authentication').hooks;

exports.before = {
  all: [],
  find: [
    auth.verifyToken(),
    auth.populateUser(),
    auth.restrictToAuthenticated()
  ],
  get: [
    auth.verifyToken(),
    auth.populateUser(),
    auth.restrictToAuthenticated(),
    auth.restrictToOwner({ ownerField: '_id' })
  ],
  create: [
    auth.hashPassword()
  ],
  update: [
    auth.verifyToken(),
    auth.populateUser(),
    auth.restrictToAuthenticated(),
    auth.restrictToOwner({ ownerField: '_id' })
  ],
  patch: [
    auth.verifyToken(),
    auth.populateUser(),
    auth.restrictToAuthenticated(),
    auth.restrictToOwner({ ownerField: '_id' })
  ],
  remove: [
    auth.verifyToken(),
    auth.populateUser(),
    auth.restrictToAuthenticated(),
    auth.restrictToOwner({ ownerField: '_id' })
  ]
};

const filterHook = () => function(hook) 
{
    console.log('AFTER=> hook para usr service')
    console.log(hook.method, hook.type)
    console.log(hook.result)
    hook.result = {_id:hook.result._id, name: hook.result.name }
    //hook.result = 'derp'
    return hook;
}

exports.after = {
  all: [hooks.pluck('_id', 'name')],
  find: [],
  get: [hooks.pluck('_id', 'name')],
  create: [],
  update: [],
  patch: [],
  remove: []
};
