const mongoose = require('mongoose');
const Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

const RecipientSchema = require('./recipient')

const schema = new Schema({
  invoiceAmount: { type: Number, required: true },
  date: { type: Date, 'default': Date.now, required: true },
  files: { type: Array },
  recipient: { type: ObjectId, ref: RecipientSchema },
  sentBy: { type: String, required: true },
  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

module.exports = schema
