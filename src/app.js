'use strict';

const path = require('path');
const serveStatic = require('feathers').static;
const favicon = require('serve-favicon');
const compress = require('compression');
const cors = require('cors');
const feathers = require('feathers');
const configuration = require('feathers-configuration');
const hooks = require('feathers-hooks');
const rest = require('feathers-rest');
const bodyParser = require('body-parser');
const socketio = require('feathers-socketio');
const authentication = require('feathers-authentication');

const middleware = require('./middleware');
const services = require('./services');

const webpack = require('webpack');

const webpackConfig = require('../webpack.config');


const app = feathers();

if(process.env.NODE_ENV == 'dev'){
  let compiler = webpack(webpackConfig);
  app.use(require('webpack-hot-middleware')(compiler));
  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    hot: true,
    displayErrorDetails : true,
    publicPath: webpackConfig.output.publicPath
  }));
}

app.configure(configuration(path.join(__dirname, '..')));

const socketConfiguration = socketio({
    path: '/socket.io/'
  }, function(io) {
      io.on('connection', function(socket) {
        console.log('client connected: ', socket.id)
      });
      io.use(function (socket, next) {
        socket.feathers.referrer = socket.request.referrer;
        next();
      });
  })
  

app.use(compress())
  .options('*', cors())
  .use(cors())
  .use(favicon( path.join(app.get('public'), 'favicon.ico') ))
  .use('/', serveStatic( app.get('public') ))
  .use('/uploads', serveStatic('uploads'))

  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: true }));
  
  app.configure(hooks())
  .configure(rest());
  
  app.configure(socketConfiguration)
  .configure(services)
  .configure(middleware);

module.exports = app;
