import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';

//REDUX
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider, connect } from 'react-redux';
import App from './components/app';
import combinedReducers from './reducers/index';

//SOCKET
import createSocketIoMiddleware from 'redux-socket.io';
import io from 'socket.io-client';
let socket = io(API_ENDPOINT);
let socketIoMiddleware = createSocketIoMiddleware(socket,"server/");



let store =  applyMiddleware(thunk,socketIoMiddleware)(createStore)(combinedReducers);
const AppComponent = connect( (state) => state )(App)

const renderApp = (store) => {
  ReactDOM.render(
    <Provider store={store} >
    <AppComponent />
     </Provider>,
    document.getElementById('container')
  );
}

injectTapEventPlugin()

renderApp(store)
