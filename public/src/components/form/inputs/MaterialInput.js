import React from 'react';
import t from 'tcomb-form';
import TextField from 'material-ui/TextField';
const TComponent = t.form.Component

import ImperativeWrapper from './ImperativeWrapper'
import MaterialList from './MaterialList'
import {List, ListItem} from 'material-ui/List';

import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import Checkbox from 'material-ui/Checkbox';

import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import Dropzone from 'react-dropzone'

class MaterialInput extends TComponent { // extend the base class
  getErrorMessage(locals) {
    if(locals.hasError)
      return locals.error
    return null
  }
  getInputValue(value, type) {
    switch(type)
    {
        case 'Number': case 'PositiveNumber':
          return parseInt(value)
        default:
          return value
    }
  }
  getSelectInput(locals,thisInput) {
    const options = locals.context.inputOptions[thisInput]
    let items = []
    for(let key of Object.keys(options))
      items.push(<MenuItem value={key} key={key} primaryText={options[key]}/>)

    return (
      <ImperativeWrapper
        ref="muiinput"
        defaultValue={locals.value || -1}
        onChange={ (val) => locals.onChange(val)}
        errorText={this.getErrorMessage(locals)}
        floatingLabelText={locals.label}
        Component={SelectField}>
        {items}
      </ImperativeWrapper>
    )
  }
  getDateInput(locals,thisInput) {
    const value = (locals.value)? new Date(locals.value)
      : new Date()
      
    return (
      <DatePicker
        value={value}
        floatingLabelText={locals.label}
        container='inline' mode='landscape'
        hintText={locals.label}
        errorText={this.getErrorMessage(locals)}
        onChange={ (ev,val) => {locals.onChange(new Date(val))} }
        ref='muiinput'
        />
    )
  }
  getValue(a,b,c) {
    const typeName = t.getTypeName(this.props.type)
    const input = this.refs.muiinput
    let value = null
    switch(typeName) {
      case 'Number': case 'PositiveNumber':
        return parseInt(input.getValue())
      case 'Date':
        return input.getDate()
      case 'Boolean':
        return input.isChecked()
      case 'File':
        return this.state.value
      default:
        return input.getValue()
    }

  }
  getFileInput(locals,thisInput) {
    const fileList = (locals.value)? 
      locals.value.map( (f,i) => {
        return (
          <ListItem key={i}
              primaryText={f.name}
              secondaryText={`${f.size} B`}  />
        )
      }) : []

    return (
      <div>
        <h3>{locals.label}</h3>
          <Dropzone
            ref='muiinput'
            accept='application/pdf'
            disablePreview={true}
            style={{width: '80%'}}
            onDrop={ files =>
              {
                let allFiles = locals.value || [] 
                allFiles.push(...files)
                locals.onChange(allFiles)
              }
            } >
                <List>
                  <ListItem key='info'
                    primaryText='Drop files here' />
                  { fileList }
                </List>          
          </Dropzone>
          <span style={{color:'red'}}>{(locals.hasError)? locals.error : null }</span>
      </div>)
  }
  getListInput(locals,thisInput) {
    const options = locals.context.inputOptions[thisInput]
    const listInputMeta = locals.typeInfo.type.meta.type.meta
    return ( <MaterialList
      ref="muiinput"
      defaultValue={locals.value}
      listInputMeta={listInputMeta}
      onChange={ (val) => {return locals.onChange(val)}}
      errorText={this.getErrorMessage(locals)}
      floatingLabelText={locals.label}
      context={locals.context} />
     )
  }
  getCheckInput(locals,thisInput) {
    const val = (locals.value == '' || !locals.value) ? false : true
    return (
      <Checkbox
        onCheck={ ev => locals.onChange(ev.target.checked) }
        label={locals.label}
        checked={val}
        style={{marginTop: '20px' }}
        labelPosition='left'
        labelStyle={{width: 'auto'}}
        ref='muiinput'
      />
    )
  }
  getTextInput(locals,thisInput) {
    const type = (locals.typeInfo.type.displayName == 'Number')?
      'number' : ((locals.typeInfo.type.displayName == 'Password')? 'password' : 'text')
    const typeName = t.getTypeName(locals.typeInfo.type)
    const multiLine = (locals.typeInfo.type.displayName == 'LongString')? true : false

    return (
      <TextField
        ref='muiinput'
        fullWidth={true}
        type={type}
        floatingLabelText={locals.label}
        hintText={locals.value|| null}
        value={locals.value || ''}
        multiLine={multiLine}
        rows={(false && multiLine)? 2 : 1}
        errorText={this.getErrorMessage(locals)}
        onChange={(ev)=> { if(ev.target){ return locals.onChange(this.getInputValue(ev.target.value, typeName))}}} />
    );
  }

  getTemplate() {
    return (locals) => {

      let thisInput = locals.path[0], input = null
      const name = t.getTypeName(locals.typeInfo.type)
      const kind = locals.typeInfo.type.meta.kind
      switch(kind){
        case 'subtype':
          switch(name){
            case 'Password': case 'LongString':
            case 'PositiveNumber': case 'Telephon':
              input = this.getTextInput(locals,thisInput)
              break
            case 'File':
              input = this.getFileInput(locals,thisInput)
              break
          }
          break

        case 'irreducible':
          switch(name){
            case 'String': case 'Number':
              input = this.getTextInput(locals,thisInput)
              break
            case 'Date':
              input = this.getDateInput(locals,thisInput)
              break
            case 'Boolean':
              input = this.getCheckInput(locals,thisInput)
              break
          }
          break
        case 'enums':
            input = this.getSelectInput(locals,thisInput)
            break
        case 'list':
            input = this.getListInput(locals,thisInput)
            break

      }

      return input

    };
  }

}

export default MaterialInput;
