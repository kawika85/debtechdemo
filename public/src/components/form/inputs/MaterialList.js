import babelPolyfill from 'babel-polyfill'
import React from 'react';
import ReactDOM from 'react-dom'

import Paper from 'material-ui/Paper';
import MaterialInputComponent from './MaterialInput'

import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

import RaisedButton from 'material-ui/RaisedButton';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import Checkbox from 'material-ui/Checkbox';


import AppBar from 'material-ui/AppBar';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';

import t from 'tcomb-form'
const Form = t.form.Form


export default class MaterialList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value: props.defaultValue || [],
      showForm: false,
      selected: [],
      edit: null
    }
    this.handle = this.handle.bind(this)
    this.onAddItem = this.onAddItem.bind(this)
    this.onDeleteItems = this.onDeleteItems.bind(this)
  }
  //recive las props mas tarde si se instancia cuando el modelo todavia no se ha cargado
  componentWillReceiveProps({defaultValue}) {
    if(defaultValue)
      this.setState({value: defaultValue})
  }
  setValue = value => this.setState({value});
  getValue = () => this.state.value;
  clearValue = () => this.setState({value: this.props.defaultValue});

  handle (ev,key,value) {
    this.props.onChange(value); this.setState({value});
  }
  onAddItem() {
    const form = this.refs.form
    const validations = form.validate()
    if(validations.errors.length == 0){
      //TODO immutable????
      const newValue = this.state.value
      if(this.state.edit)
        newValue[this.state.edit] = form.getValue()
      else
        newValue.push(form.getValue())

      this.setState({
        value: newValue,
        showForm: false,
        edit: null
      })
    }
  }
  onDeleteItems() {
    let newValue = this.state.value
    const toRemove = this.state.selected.map( (i) => this.state.value[i])
    toRemove.forEach( (v) => {
      const i = newValue.findIndex(val => v == val)
      newValue.splice(i,1)
    })
    this.setState({
      value: newValue,
      selected: []
    })
  }
  //DRY!
  getFields(keys) {
    let ret = {}
    for(let key of keys)
      ret[key] = {factory: MaterialInputComponent}
    return ret
  }
  getItems(items, keys) {
    if(items && items.length > 0)
      return items.map(
        (item, i) => {
          return this.getRow(keys, item, i)
      })
    else
      return []
  }
  getRow(keys, item, i = -1) {
    const Column = (item)? TableRowColumn : TableHeaderColumn
    const selected = (this.state.selected.length > 0 &&
      this.state.selected.findIndex((v) => v === i) > -1)? true : false

    let row = (item)?
      [<Column key={69}>
        <Checkbox defaultChecked={selected} />
      </Column>] : [<Column key={69}/>]
    row.push(...keys.map(
      (el, j) => (
        <Column
          key={j} >
          {(item)? item[el] : el}
        </Column> )))
    row.push(
      <Column key={70}>
        {(item)? <EditIcon /> : null}
      </Column>
    )

    return <TableRow selectable={true} key={i}>{row}</TableRow>
  }
  getToolbar() {
    return (
      <Toolbar >
        <ToolbarGroup  float="right">
          {
            (!this.state.showForm)?
              <RaisedButton
                label="Add" secondary={true}
                onTouchTap={ () => {this.setState({showForm: true})}}/>
            : null
          }
          {
            (this.state.selected.length > 0)?
              <RaisedButton
                label="Delete" primary={true}
                onTouchTap={this.onDeleteItems}/>
            : null
          }
        </ToolbarGroup>
       </Toolbar>
     )
  }
  getLayoutColumns() {
    if(this.state.showForm)
      return [5,6]
    else
      return [12,0]
  }
  componentDidUpdate() {
    //autofocus, find a better way...
    if(this.refs.form){
      const formInputs = this.refs.form.refs.input
      const firstInput = Object.keys(formInputs.refs)[0]
      const muiInput = formInputs.refs[firstInput].refs.muiinput
      const node = ReactDOM.findDOMNode(muiInput)
      node.children[1].focus()
    }
  }
  render() {
    const {Component, children,
      floatingLabelText,defaultValue,listInputMeta,  ...other} = this.props;
    const metaPropsKeys = Object.keys(listInputMeta.props)
    const header = this.getRow(metaPropsKeys)
    const items = this.getItems(this.state.value, metaPropsKeys)
    let val = null
    if(this.state.value){
      val = { value: this.state.value[this.state.edit]}
    }
    const options = {
      fields: this.getFields(metaPropsKeys)
    }
    const [colLeft, colRight] = this.getLayoutColumns()
    return (
      <div >
        {this.getToolbar()}
          <div className='row'>
          <div className={`col-xs-${colLeft}`}>
              <Table
                multiSelectable={true}
                onCellClick={
                  (rowI,cellI, ev) => {
                    //console.log('---->',metaPropsKeys.length, cellI)
                    if(cellI == 1){
                      let newSelected = this.state.selected
                      const i = newSelected.findIndex((v)=> v == rowI)
                      if(i > -1)
                        newSelected.splice(i,1)
                      else
                        newSelected.push(rowI)
                      this.setState({selected: newSelected})
                    }
                    if(cellI == metaPropsKeys.length + 2){
                      //console.log('EDIT!!!', rowI)
                      this.setState({showForm: true, edit: rowI})
                    }
                    ev.stopPropagation()
                  }
                }
                onRowSelections={ (s) => { this.setState({selected: s}) } }>
                <TableHeader adjustForCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                    {header}
                </TableHeader>
                <TableBody
                  displayRowCheckbox={false}
                  deselectOnClickaway={false}
                  showRowHover={true}>
                  {items}
                </TableBody>
              </Table>
          </div>
          {
            (colRight > 0)?
              <div className={`col-xs-${colRight}`}>
                <Paper zDepth={3} >
                  <AppBar
                  title={`Add ${this.props.floatingLabelText}`}
                  //onTitleTouchTap={handleTouchTap}
                  iconElementLeft={<IconButton><NavigationClose /></IconButton>}

                />


                <Form ref='form'
                {...val}
                type={t.struct(listInputMeta.props)}
                options={options} context={this.props.context }/>
                <Toolbar >
                  <ToolbarGroup firstChild={false} float="left">
                  <RaisedButton
                    label="Save" secondary={true}
                    onTouchTap={ this.onAddItem }/>
                  <RaisedButton
                    label="Cancel"
                    onTouchTap={ () => {this.setState({showForm: false})}}/>
                </ToolbarGroup>
                </Toolbar >

              </Paper>

              </div>
            : null
          }
        </div>
    </div>
    )
  }
}
