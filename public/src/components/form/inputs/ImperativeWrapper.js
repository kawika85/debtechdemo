import React from 'react';

export default class ImperativeWrapper extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: props.defaultValue};
    this.handle = this.handle.bind(this)
  }

  setValue = value => this.setState({value});
  getValue = () => this.state.value;
  clearValue = () => this.setState({value: this.props.defaultValue});

  handle (ev,key,value) {
    this.props.onChange(value); this.setState({value});
  }

  render() {
    const {Component, children, ...other} = this.props;
    return <Component {...other} value={this.state.value} onChange={this.handle}>{children}</Component>;
  }
}
