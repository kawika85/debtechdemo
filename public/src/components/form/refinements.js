import t from 'tcomb-form';

const Password = t.refinement(t.String, (n) => true, 'Password');

const Integer = t.refinement(t.Number, (n) => n % 1 === 0, 'Integer');

const telRegEx = /^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/

const Telephon = t.refinement(t.String, (n) => telRegEx.test(n), 'Telephon');
Telephon.getValidationErrorMessage = (value, path, context) => {
  return 'Please enter a valid  Telephon number '
};

const LongString = t.refinement(t.String, (n) => (n != ''), 'LongString');
LongString.getValidationErrorMessage = (value, path, context) => {
  return 'Please enter a valid  string '
};

const File = t.refinement(t.Array, (n) => true, 'File');
File.getValidationErrorMessage = () => 'Please upload a file '

const PositiveNumber = t.refinement(t.Number, (n) => n >= 0, 'PositiveNumber');
PositiveNumber.getValidationErrorMessage = (value, path, context) => {
  return 'Please enter a valid  positive number '
};


export {PositiveNumber, Integer, File, LongString, Password, Telephon}


