import React, {PropTypes, Component} from 'react'

import RaisedButton from 'material-ui/RaisedButton';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';


class FormToolbarComponent extends Component {
  getButtonsProps(model,changed) {
    const loadingState = (model.update && !changed) ? model.update.loadingState : 0
    const disabled = (loadingState == 2 && !changed)? true : false
    const saveTexts = ['Save', 'Saving...', 'Saved']
    const cancelText = (loadingState == 2)? 'Back' : 'Cancel'
    return (
      {
        saveText: saveTexts[loadingState],
        saveDisabled: disabled,
        cancelText: cancelText
      }
    )
  }
  render() {
    const {model, changed, routePrefix, plural} = this.props
    const ButtonsProps = this.getButtonsProps(model,changed)
    let prevUrl = ''
    if(routePrefix)
      prevUrl = (plural)?`#/${routePrefix}/${plural}` : `#/${routePrefix}`
    else
      prevUrl = `#/${plural}`
    return (
      <Toolbar>
        <ToolbarGroup firstChild={true} float='left'>
          {
            (true || this.props.auth.uid)?
              <RaisedButton
                label={ButtonsProps.saveText} disabled={ButtonsProps.saveDisabled}
                onClick={this.props.onFormSubmit} secondary={true} ref='saveButton'/>
              : null
          }
          <RaisedButton label={ButtonsProps.cancelText} linkButton={true} href={prevUrl} />
        </ToolbarGroup>
        {
          (this.props.id && (true || this.props.auth.uid) )?
            <ToolbarGroup firstChild={true} float='right'>
              <RaisedButton label='Delete' primary={true} onClick={this.props.onDelete}/>
            </ToolbarGroup>
          :
            null
        }
      </Toolbar>
    )
  }
}

export default FormToolbarComponent
