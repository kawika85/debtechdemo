import React, {PropTypes, Component} from 'react'

import MaterialInputComponent from './inputs/MaterialInput'

import t from 'tcomb-form'
const Form = t.form.Form
const myTemplate = t.form.Form.templates.list.clone()

class TFormComponent extends Component {
  constructor() {
    super()
    this.state = {data: null}
  }
  submitForm() {
    const validations = this.refs.form.validate()
    if(validations.errors.length == 0)
      return this.refs.form.getValue()
    else{
        console.warn('validations.errors',validations.errors)
        return false
    }
  }
  getFields() {
    let ret = {}
    for(let key of Object.keys(this.props.schema.schema))
      ret[key] = {factory: MaterialInputComponent}
    return ret
  }
  validate() {
    return this.refs.form.validate()
  }
  getValue() {
    return this.refs.form.getValue()
  }
  resetValue() {
    this.setState({data: null})
  }
  render() {
    let options = {
      fields: this.getFields()
    }

    const context = {
      inputTypes: this.props.inputTypes,
      inputOptions: this.props.inputOptions
    }
    const {responseState, model} = this.props
    const formValue = this.state.data || this.props.model.data
    
    return (
          <form onSubmit={this.onSubmit}>
            <Form ref='form' context={context}
              type={t.struct(this.props.schema.schema)}
              onChange={ (val) => {
                this.setState({data: val})
                if(this.props.onChange)
                  this.props.onChange(val)
                }}
              value={formValue} options={options} />
          </form>
    )
  }
}

export default TFormComponent
