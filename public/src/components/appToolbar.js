import React from 'react';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import FlatButton from 'material-ui/FlatButton';

import UserToolbar from './appToolbar/userToolbar'

export default class AppToolbar extends React.Component {

  render() {
    const {dispatch, auth, onChangeTab, tab} = this.props
    return (
      <div>
        <UserToolbar auth={auth} dispatch={dispatch} />
        {
          (auth.user)?
            <Toolbar>
              <ToolbarGroup lastChild={true} >
                <FlatButton
                  primary={(tab=='new-invoice')? true: false}
                  label="Create Invoice" onTouchTap={() => onChangeTab('new-invoice') } />
                <FlatButton
                  primary={(tab=='invoices')? true: false}
                  label="Invoices" onTouchTap={() => onChangeTab('invoices') }/>
              </ToolbarGroup>
            </Toolbar> : null
          }
      </div>
    );
  }
}
