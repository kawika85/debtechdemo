import React, {PropTypes, Component} from 'react'

import {ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Face from 'material-ui/svg-icons/action/face';
import {grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';

import TimeAgo from 'react-timeago'



class ItemComponent extends Component {
  
  componentDidMount() {
    const { dispatch } = this.props
  }
  
  render() {
    const {item, onSelect, index} = this.props
    const date = new Date(item.date),
      formattedDate = date.toLocaleDateString()
    return (
    <div>
      <ListItem
          primaryText={`${item.invoiceAmount}€ for the ${formattedDate}`}
          secondaryText={
            <p>
              <span style={{color: darkBlack}}>{item.user.name}</span> --
              <TimeAgo date={item.createdAt} minPeriod={10} />
            </p>
          }
          onTouchTap={ (ev) => onSelect(index)}
          secondaryTextLines={2}
        />
        <Divider inset={true} />
      </div>
    )
  }

}


export default ItemComponent;
