import React, {PropTypes, Component} from 'react'

import {List, ListItem} from 'material-ui/List';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';

import t from 'tcomb-form'
import TForm from '../form/TCForm'
import {LongString, PositiveNumber, Telephon} from '../form/refinements'

const RecipientSchema = {
  name: LongString,
  surname: LongString,
  address: LongString,
  phone: Telephon
}

class RecipientDialog extends Component {
  constructor() {
    super()
    this.state = {
        showRecipient: false
    }
  }
  addRecipient = () => {
    const val = this.refs.recipientform.submitForm()
    if(val){
      this.setState({showRecipient: false})
      this.props.onAddRecipient(val)
    }
  }
  handleOpen = () => {
    this.setState({showRecipient: true});
  }
  handleClose = () => {
    this.setState({showRecipient: false});
  }
  getRecipientDetails(recipient) {
    const items = Object.keys(recipient).map(function (key,i) {
      return <ListItem key={i} primaryText={recipient[key]} secondaryText={key} />
    });
    return (
        <List >
          {items}
        </List>
    )
  }
  render() {
    const {recipient} = this.props
    const recipientActions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label={(recipient)? 'Edit': 'Add'}
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.addRecipient}
      />,
    ];
    
    return (
        <Paper zDepth={2} >

          {
            (recipient)? 
              this.getRecipientDetails(recipient) : null
          }
          <FlatButton 
            style={{float: 'right'}}
            label={(recipient)? 'Edit Recipient': 'Add Recipient'}
            onTouchTap={this.handleOpen} />
          <Dialog
            title="Add a recipient"
            actions={recipientActions}
            open={this.state.showRecipient}
            onRequestClose={this.handleClose}
          >
            <TForm 
              schema={{schema: RecipientSchema}}
              ref='recipientform'
              errorMessage={null}
              model={{data:recipient}} />
          </Dialog>
      </Paper>
    )
  }

}


export default RecipientDialog;

