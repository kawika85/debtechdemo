import React, {PropTypes, Component} from 'react'

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Subheader from 'material-ui/Subheader';


import t from 'tcomb-form'
import TForm from '../form/TCForm'
import {LongString, PositiveNumber, File} from '../form/refinements'
import RecipientDialog from './recipientDialog'


const InvoiceSchema = {
  invoiceAmount: PositiveNumber,
  date: t.Date,
  files: File
}

class MessageForm extends Component {
  constructor() {
    super()
    this.post = this.post.bind(this)
    this.state = {
        recipient: null
    }
  }
  post() {
    const invoiceData = this.refs.form.submitForm()
    if(invoiceData && this.state.recipient){
      const data = {...invoiceData, recipient: JSON.stringify(this.state.recipient)}
      this.props.onPostForm(data)
    }
  }
  
  render() {
    const actions = [
      <RaisedButton
        label="Save Invoice"
        primary={true}
        onTouchTap={this.post}
        key={2}
      />
      ,
      <RaisedButton
        label="Cancel"
        primary={false}
        onTouchTap={this.clear}
        key={1}
      />
    ];
    const columnStyle = {width: '50%', display: 'inline-block', verticalAlign: 'top'}
    return (
      <div>
        <div style={columnStyle} >
          <h3>Invoice Details</h3>
          <TForm 
            schema={{schema: InvoiceSchema}}
            ref='form'
            errorMessage={null}
            model={{data: null}} />
        </div>
        <div style={columnStyle} >
          <h3>Recipient Details</h3>
          
          <RecipientDialog 
            recipient={this.state.recipient}
            onAddRecipient={ (val) => this.setState({recipient: val}) }
          />
        </div>
        
        {actions}
      </div>
    )
  }

}


export default MessageForm;

