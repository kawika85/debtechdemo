import React, {PropTypes, Component} from 'react'
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import {grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';

import Dialog from 'material-ui/Dialog';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import Item from './item'

class ListComponent extends Component {
  
  constructor() {
    super()
    this.onItemSelect = this.onItemSelect.bind(this)
    this.state = { showDetailsFrom: null}

  }
  componentDidMount() {
    const { dispatch } = this.props
  }
  onItemSelect(i) {
    this.setState({
        showDetailsFrom: i
    })
  }
  handleClose = () => {
    this.setState({showDetailsFrom: null});
  }
  getInvoiceDetails(item) {
    const {recipient, files} = item
    const date = new Date(item.date)
    const DetailItem = (text1, text2 = null, i ) => {
      return <ListItem key={i}
          primaryText={text1}
          secondaryText={text2}  />
    }
    const recipientDetails = ['name', 'surname', 'address', 'phone'].map( (d,i) => {
        return( DetailItem(recipient[d], d, i))
    })
    const filesList = files.map( (f,i) => {
        return <ListItem
          key={i}
          primaryText={f}
          onTouchTap={ (ev) => window.open(`uploads/${f}`)}  />
        
    })
    return (
      <div>
        <div style={{width: '50%', float: 'left'}} >
          <h4>Recipient</h4>
          <List>
              {recipientDetails}
          </List>
        </div>
        <div style={{width: '50%', float: 'left'}} >
          <h4>{`Amount: ${item.invoiceAmount} €`}</h4>
          <h4>{`Expires: ${date.toLocaleDateString()}`}</h4>
          <h4>Files</h4>
          <List>
              {filesList}
          </List>
        </div>
      </div>
    )
  }
  getDialog(){
    if(this.state.showDetailsFrom != null){
      const {items} = this.props 
      const item = items[ this.state.showDetailsFrom ]
      return(
        <Dialog
          title="Invoice Details"
          actions={[
            <FlatButton
              label="Ok"
              primary={true}
              onTouchTap={this.handleClose}
            />
          ]}
          open={true}
          onRequestClose={this.handleClose}
        >
          { this.getInvoiceDetails(item) }
        </Dialog>
      )
    }
    return null
  }
  render() {
    const {items} = this.props
    if(!items)
      return null
      
    const listItems = items.map( (i, n) => {
      return<Item onSelect={this.onItemSelect} index={n} item={i} key={n}/>
      }
    )
    return (
      <div>
        <List>
          <Subheader>Invoices</Subheader>
          {listItems}
          {this.getDialog()}
        </List>
      </div>
    )
  }

}


export default ListComponent;
