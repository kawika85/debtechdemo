
import React, {PropTypes, Component} from 'react'
import {List, ListItem} from 'material-ui/List';

import Subheader from 'material-ui/Subheader';
import AppBar from 'material-ui/AppBar';

import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';

import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';

import Face from 'material-ui/svg-icons/action/face';
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar';

import {logoutAction} from '../../actions/auth'

import LoginDialog from './loginDialog'

class UserToolbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginModal: false
    };
    this.loginModalOpen = this.loginModalOpen.bind(this)
    this.loginModalClose = this.loginModalClose.bind(this)
  }
  loginModalOpen = () => {
    this.setState({loginModal: true});
  };
  loginModalClose = () => {
    this.setState({loginModal: false});
  };

  render() {
    const {auth, dispatch} = this.props
    return (
    <div>
      <AppBar title='Debitech'>
        <ToolbarGroup lastChild={true} >
        {
          (auth.user)?
            <IconMenu
              iconButtonElement={
                <IconButton
                  iconStyle={{width: 30, height: 30}}
                ><Face /></IconButton>
              }
            >
              <MenuItem
                onTouchTap={ (ev)=> dispatch(logoutAction())}
                primaryText="Sign out" />
            </IconMenu>
          : <FlatButton label="Login" onTouchTap={this.loginModalOpen}/>
        }

        </ToolbarGroup>

      </AppBar>
        {
          (!auth.user)?
            <LoginDialog
              dispatch={dispatch}
              auth={auth}
              open={this.state.loginModal}
              loginModalClose={this.loginModalClose} />
            : null
        }
      </div>
    )
  }


}


export default UserToolbar;
