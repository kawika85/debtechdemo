import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import {loginAction, registerloginAction} from '../../actions/auth'

import t from 'tcomb-form'
import TForm from '../form/TCForm'

import {Password} from '../form/refinements'
const LoginSchema = {
  name: t.String,
  password: Password
}
export default class DialogExampleSimple extends React.Component {
  constructor() {
    super()
    this.signIn = this.signIn.bind(this)
    this.register = this.register.bind(this)
  }
  signIn() {
    const val = this.refs.form.submitForm()
    const {dispatch} = this.props
    dispatch(loginAction(val.name, val.password))
  }
  register() {
    const val = this.refs.form.submitForm()
    const {dispatch} = this.props
    dispatch(registerloginAction(val.name, val.password))
  }
  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.props.loginModalClose}
      />,
      <FlatButton
        label="Register"
        primary={false}
        onTouchTap={this.register}

      />,
      <FlatButton
        label="Login"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.signIn}
      />,
    ];
    const {auth} = this.props
    const errorMessage = auth.loginMessage
    return (
      <div>
        <Dialog
          title="Sign In"
          actions={actions}
          modal={false}
          open={this.props.open}
          onRequestClose={this.props.loginModalClose}
        >
          <TForm
            schema={{schema: LoginSchema, fields: {password: {type: 'password'} }}}
            toolbar={false}
            ref='form'
            errorMessage={errorMessage}
            model={{data:''}} />
        </Dialog>
      </div>
    );
  }
}
