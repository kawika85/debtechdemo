import React, {PropTypes, Component} from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import {loadLocalUserAction} from '../actions/auth'
import {getInvoicesAction, postInvoiceAction} from '../actions/invoices'

import AppToolbar from './appToolbar';
import InvoiceForm from './invoices/form'
import InvoicesList from './invoices/list'
import CircularProgress from 'material-ui/CircularProgress';


class App extends Component {

  componentWillMount() {
    const {dispatch} = this.props
    dispatch(loadLocalUserAction())
  }
  fetch() {
    const {dispatch} = this.props
    dispatch(getInvoicesAction())
  }
  getContent(){
    const {dispatch, invoices, mpd, app} = this.props
    switch(app.tab){
      case 'new-invoice':
          return (
            <InvoiceForm
              onPostForm={ (val) => {
                  dispatch(postInvoiceAction(val))
                }
              }
            />
            )
      case 'invoices':
          return (
            <InvoicesList items={invoices.list} dispatch={dispatch} />
            )
      default:
        return null
    }
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.auth.user && !this.props.auth.user)
      this.fetch()
  }
  getLoadingDiv() {
    const {app} = this.props
    if(app.waiting){
      const circularStyle = { margin: '250 auto', display: 'inherit'}
      const loadingDivStyle = {
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: 99,
        backgroundColor: 'white',
        opacity: 0.5
      }

      return (
        <div style={loadingDivStyle} >
          <CircularProgress style={circularStyle} size={2} />
        </div>
      )
    }else
      return null

  }
  render() {
    const {dispatch, auth, local, app} = this.props
    const loadingDiv = this.getLoadingDiv()

    return (
     <MuiThemeProvider>
      <div>
        { loadingDiv }
        <AppToolbar
          tab={app.tab}
          onChangeTab={ (tab) => dispatch({type:'APP:CHANGE_TAB', tab: tab})}
          dispatch={dispatch} auth={auth} />
        { (auth.user)? this.getContent() : null}
      </div>
      </MuiThemeProvider>
    )
  }

}
export default App
