const endpoint = API_ENDPOINT
import localforage from 'localforage'

function checkResponseStatus(response){
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}

export function transformCreateResponse (response) {
   return response.json()
    .then( json => {
      return {
        response: json,
        status: (response.status == 201)? 'SUCCESS' : 'FAILED'
      }
    })
}

export function getMessages() {
  return function(dispatch) {
    const params =`?$limit=100&$sort[createdAt]=-1`
    fetch(`${endpoint}messages${params}`, {
      method: 'get'
    })
    .then(response => response.json())
    .then( json => {
      dispatch({
        type: 'GET_MESSAGES',
        val: json
      });
    }).catch(err => {
    // Error: handle it the way you like, undoing the optimistic update,
    //  showing a "out of sync" message, etc.
    });
  return null;
  }
}


export function authRequest(url, type, body, options) {

  return localforage.getItem('user').then( user => {
      //const authBody = `${body}&token=${user.token}`
      return request(url, type, body, user.token, options)
    })
  //authBody = `${body}&=`
}
export function request(url, type, body, token = null, options = {}) {
  let formData = null
  let headers = {
      'Accept': 'application/json'
  }
  if(token)
    headers['Authorization'] = `Bearer ${token}`

  if(options.multipart){
    //if multipart, we let the browser generate the content type header
    formData = new FormData()
    Object.keys(body).forEach(function (key) {
      const val = body[key]
      if(key == 'files'){
        for(let i in val){
          formData.append(`${key}-${i}`, val[i], val[i].name);
        }
      }else
        formData.append(key, val);
    });
  }else
  {
    if(type == 'POST')
      headers['Content-Type'] = 'application/json'
    else
      headers['Content-Type'] = 'application/x-www-form-urlencoded'
  }

  return fetch(url, {
    method: type,
    headers: headers,
    body:  formData || body
  })
}
