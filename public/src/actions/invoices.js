import {request, authRequest, transformCreateResponse} from './req'
import localforage from 'localforage'

const endpoint = API_ENDPOINT

export function postInvoiceAction( content ) {
  return function(dispatch) {
    dispatch({
      type:'APP:SET_WAITING',
      waiting: true
    })
    return authRequest(
        `${endpoint}invoices`,
        'post',
        content,
        { multipart: true })
      .then( response => response.json() )
      .then( data => {
        dispatch({
          type: 'INVOICE:CREATED',
          data: data
        })
        dispatch({
          type:'APP:CHANGE_TAB',
          tab: 'invoices'
        })
        dispatch({
          type:'APP:SET_WAITING',
          waiting: false
        })
      })
    }
}

export function getInvoicesAction( content ) {
  return function(dispatch) {
    return authRequest(
    `${endpoint}invoices?$sort[createdAt]=-1`,
    'get')
    .then( response => response.json() )
    .then( data => {
      dispatch({
        type: 'INVOICE:GET_LIST',
        data: data
      });

    })
  }
}
