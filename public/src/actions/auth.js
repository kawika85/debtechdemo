import {request, transformCreateResponse} from './req'
import localforage from 'localforage'

const endpoint = API_ENDPOINT


const login = (name, password) => request(
  `${endpoint}auth/local`,
  'post',
  `name=${name}&password=${password}`)

const register = (name, password) => request(
  `${endpoint}users`,
  'post',
  `name=${name}&password=${password}`)

const loadLocalUser = () => localforage.getItem('user')


const registerloginRequest = (dispatch, email, password) => {
  register(email, password)
  .then(transformCreateResponse)
  .then( data => {
    dispatch({
      type: 'REGISTER',
      status: data.status,
      response: data.response
    });
    if(data.status == 'SUCCESS'){
      loginRequest(dispatch, email, password)
    }
  })
  .catch(err => {
    console.log('CATCH!!!', err)
  });
}

const loginRequest = (dispatch, email, password) => {
  login(email, password)
  .then(transformCreateResponse)
  .then( data => {
    const usr = {
      token: data.response.token,
      user: data.response.data.name
    }

    localforage.setItem('user', usr).then( ()=> {
      dispatch({
        type: 'LOGIN',
        status: data.status,
        user: usr
        });
    })

  }).catch(err => {
    console.log('CATCH!!!', err)
  });
 }

const logoutRequest = (dispatch, email, password) => {
   //TODO implement token invalidation on the server
   //https://github.com/feathersjs/feathers-authentication/blob/master/src/services/token.js
   localforage.removeItem('user').then( ()=> {
     dispatch({
       type: 'REMOVE_LOCAL_USER'
       });
   })
  }

export function registerloginAction( email, password) {
  return function(dispatch) {
    registerloginRequest(dispatch, email, password)
    return null;
  }
}

export function loginAction( email, password) {
  return function(dispatch) {
    loginRequest(dispatch, email, password)
    return null;
  }
}

export function logoutAction() {
  return function(dispatch) {
    logoutRequest(dispatch)
    return null;
  }
}

export function loadLocalUserAction() {
  return function(dispatch) {
    loadLocalUser().then( user => {
      dispatch({
        type: 'LOAD_LOCAL_USER',
        user: user
      });
    })
    return null;
  }

}
