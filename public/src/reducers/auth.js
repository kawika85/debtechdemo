const initialState = {
  loginState: null,
  loginMessage: null,
  user: null
}

function messagesApp(state = initialState, action) {
  switch (action.type) {
    case 'LOAD_LOCAL_USER':
        if(action.user){
          return Object.assign({}, state, {
              loginState: 'LOGGED_IN',
              loginMessage: null,
              user: action.user
            })
        }else
          return state
      break
    case 'REMOVE_LOCAL_USER':
        return Object.assign({}, state, initialState)
      break
    case 'LOGIN':
      if( action.status == 'SUCCESS'){
        return Object.assign({}, state, {
            loginState: 'LOGGED_IN',
            loginMessage: null,
            user: action.user
          })
      }else
      {
        return Object.assign({}, state, {
            loginState: 'LOGGED_FAILED',
            loginMessage: action.response.message,
            user: null
          })
      }
      break
    case 'REGISTER':
      if( action.status == 'SUCCESS'){
        return Object.assign({}, state, {
            loginState: 'REGISTERED_SUCCESS',
            loginMessage: null,
            user: null
          })
      }else
      {
        return Object.assign({}, state, {
            loginState: 'REGISTERED_FAILED',
            loginMessage: action.response.message,
            user: null
          })
      }
    default:
      return state
  }
}

export default messagesApp
