import { combineReducers } from 'redux'
import auth from './auth'
import invoices from './invoices'
import app from './app'

export default combineReducers({
  app,
  auth,
  invoices
})
