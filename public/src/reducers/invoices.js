const initialState = {
  list: null
  }

function invoicesState(state = initialState, action) {
  switch (action.type) {
    case 'INVOICE:GET_LIST':
      return Object.assign({}, state, {
              list: action.data.data
            })
    case 'INVOICE:NEW':
      const {list} = state
      return Object.assign({}, state, {
              list: [action.data, ...list]
            })
    default:
      return state
  }
}

export default invoicesState

