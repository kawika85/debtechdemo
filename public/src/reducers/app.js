const initialState = {
  tab : 'new-invoice',
  waiting: false
}

function messagesApp(state = initialState, action) {
  switch (action.type) {
    case 'APP:CHANGE_TAB':
        return Object.assign({}, state, {
            tab: action.tab
        })
      break
    case 'APP:SET_WAITING':
          return Object.assign({}, state, {
              waiting: action.waiting
          })
        break
    default:
      return state
  }
}

export default messagesApp
