# File Upload Demo

>

## About

Demo App using Feathers + React

## Check
+ You need mongo running
+ Configuration in config/default.json and config/production.json. Sample provided
+ Create uploads folder on the root of the app. Make sure permissions are ok

## Getting Started

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed. Even better [nvm](https://github.com/creationix/nvm)
2. Install your dependencies

    ```
    cd path/to/app; npm install
    ```

3. Start developing with webpack dev server

    ```
    npm run start:dev
    ```
4. Or build the client bundle and start the regular server. Specify endpoint through argument, use localhost instead

    ```
    npm run build:webpack -- --endpoint http://69.69.167.132:3030/
    npm start
    ```
